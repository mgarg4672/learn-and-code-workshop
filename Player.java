package Workshop;

public class Player {
long playerId;
String playerName;
public Player(long playerId, String name) {
    this.playerId = playerId;
    this.playerName = name;
}

public long getPlayerId() {
    return playerId;
}

public String getName() {
    return playerName;
}
}

