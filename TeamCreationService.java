package Workshop;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.json.simple.*;
import org.json.simple.parser.*;
import org.json.simple.JSONArray;

public class TeamCreationService {
	
	public TeamList createTeam(Game game)
	{
		ArrayList<Team> teamList = getTeamListFromGame(game);
		TeamList team = new TeamList();
		team.items = teamList;
		team.total = teamList.size();
		return team;
	}
	
	private  ArrayList<Team> getTeamListFromGame(Game game)
	{
		ArrayList<Team> teamList = new ArrayList<Team>();
		long gameType = game.gameType;
		ArrayList<Player> playerList= game.playerList;
		int playerCount = 0;
		long teamId = 1;
		char teamName = 'A';
		while(playerCount < playerList.size())
		{
			Team team = new Team();
			ArrayList<Player> teamPlayerList = new ArrayList<Player>();
			
			ArrayList gameTypeWithNumberOfPlayers;
			Object numberOfPlayers = gameTypeWithNumberOfPlayers.get((int)gameType);
			int count = 0;
			int sizeDiff = playerList.size() - playerCount;
			if(sizeDiff >= numberOfPlayers)
			{
			while(count < numberOfPlayers)
			{
				teamPlayerList.add(playerList.get(playerCount));
				count ++;
				playerCount++;
			}
			team.id = teamId;
			team.name = "Team - "+teamName;
			team.playerList = teamPlayerList;
			team.gameType = gameType;
			teamList.add(team);
			teamId = teamId+1;
			teamName += 1;
			}
			else
			{
			playerCount++;
			}
		}
		return teamList;
	}
	public void prepareTeamJSON(long gameType, List<Player> playerList ) throws FileNotFoundException
	{
		JSONObject jo = new JSONObject();
		
		jo.put("total", playerList.size());
		JSONArray playerArray = new JSONArray();
		JSONArray teamArray = new JSONArray();
		
		for(int i=0;i < playerList.size(); i++) {
			Map playerMap = new LinkedHashMap(2);
			System.out.println(playerList.get(i).playerName);
			playerMap.put("playerId", playerList.get(i).playerId);
			playerMap.put("name", playerList.get(i).playerName);
			playerArray.add(playerMap);
		}
		Map m = new LinkedHashMap(4);
		int teamCount = 0;
		m.put("id", teamCount);
		m.put("name", "Team - " + teamCount);
		m.put("gametype", gameType);
		m.put("players", playerArray);
		teamCount = teamCount ++ ;
		teamArray.add(m);
		jo.put("teams", teamArray);
		
	}
	public void jsonRead() throws FileNotFoundException, IOException, ParseException{
		JsonReadWrite obj1 = new JsonReadWrite();
		// parsing file "JSONExample.json"
		Object obj = new JSONParser().parse(new FileReader("JSONExample.json"));
		JSONObject jo = (JSONObject) obj;
		long gameType = (long) jo.get("gameType");
		System.out.println(gameType);
		
	}
}
