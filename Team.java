package Workshop;

import java.util.ArrayList;

public class Team {

	Long id;
	String name;
	Long gameType;
	ArrayList<Player> playerList;
	
	public ArrayList<Player> getPlayerList() {
		return playerList;
	}
	public void setPlayerList(ArrayList<Player> playerList) {
		this.playerList = playerList;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getGameType() {
		return gameType;
	}
	public void setGameType(Long gameType) {
		this.gameType = gameType;
	}
	
}